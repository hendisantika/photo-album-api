package com.hendisantika.photoalbumapi.controller;

import com.hendisantika.photoalbumapi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:51
 */
@RestController
@RequestMapping("${photoalbum.route.files}")
public class FileController {

    @Autowired
    FileService fileService;

    @RequestMapping(value = "{filename:.+}", method = RequestMethod.GET)
    public byte[] serveFile(@PathVariable("filename") String filename) {
        return this.fileService.serveFile(filename);
    }

}