package com.hendisantika.photoalbumapi.domain.entity;

import com.hendisantika.photoalbumapi.domain.base.DomainBase;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:25
 */
@Entity
@Table(name = "files")
public class File extends DomainBase {

    //private static final long serialVersionUID = ;
    private Long id;
    private String filename;
    private Date createdDate;
    private byte[] data;

    public File() {
        super();
    }

    public File(String filename, Date createdDate, byte[] data) {
        this.setFilename(filename);
        this.setCreatedDate(createdDate);
        this.setData(data);
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "files_seq")
    @SequenceGenerator(name = "files_seq", sequenceName = "files_seq", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "filename")
    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "data", columnDefinition = "blob")
    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}