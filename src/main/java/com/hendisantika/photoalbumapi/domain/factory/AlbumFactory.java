package com.hendisantika.photoalbumapi.domain.factory;

import com.hendisantika.photoalbumapi.domain.entity.Album;
import com.hendisantika.photoalbumapi.model.request.AlbumRequest;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:27
 */
@Component
public class AlbumFactory implements FactoryBean<Album> {

    public Album create(AlbumRequest albumRequest) {
        return new Album(
                albumRequest.getTitle(),
                new Date(),
                albumRequest.getCoverPhotoId()
        );
    }

    public List<Album> create(List<AlbumRequest> albumRequestList) {
        List<Album> albumList = new LinkedList<Album>();
        if (albumRequestList != null) {
            albumRequestList.forEach(a -> albumList.add(this.create(a)));
        }
        return albumList;
    }

    @Override
    public Album getObject() {
        return new Album();
    }

    @Override
    public Class<Album> getObjectType() {
        return Album.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}