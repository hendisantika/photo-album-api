package com.hendisantika.photoalbumapi.domain.factory;

import com.hendisantika.photoalbumapi.domain.entity.File;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:28
 */
@Component
public class FileFactory implements FactoryBean<File> {

    public File create(MultipartFile multipartFile) throws Exception {
        return new File(
                multipartFile.getOriginalFilename(),
                new Date(),
                multipartFile.getBytes()
        );
    }

  /*public List<File> create(List<MultipartFile> multipartFileList) throws Exception {
    List<File> fileList = new LinkedList<File>();
    if (multipartFileList != null) {
      multipartFileList.forEach(f -> fileList.add(this.create(f)));
    }
    return fileList;
  }*/

    @Override
    public File getObject() {
        return new File();
    }

    @Override
    public Class<File> getObjectType() {
        return File.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}


