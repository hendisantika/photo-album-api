package com.hendisantika.photoalbumapi.domain.factory;

import com.hendisantika.photoalbumapi.domain.entity.Photo;
import com.hendisantika.photoalbumapi.model.request.PhotoRequest;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:29
 */
@Component
public class PhotoFactory implements FactoryBean<Photo> {

    public Photo create(PhotoRequest photoRequest) {
        return new Photo(
                photoRequest.getTitle(),
                new Date(),
                photoRequest.getFilePath(),
                photoRequest.getAlbumId()
        );
    }

    public List<Photo> create(List<PhotoRequest> photoRequestList) {
        List<Photo> photoList = new LinkedList<Photo>();
        if (photoRequestList != null) {
            photoRequestList.forEach(p -> photoList.add(this.create(p)));
        }
        return photoList;
    }

    @Override
    public Photo getObject() {
        return new Photo();
    }

    @Override
    public Class<Photo> getObjectType() {
        return Photo.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}