package com.hendisantika.photoalbumapi.model.request;

import com.hendisantika.photoalbumapi.model.ModelBase;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:18
 */
public class AlbumRequest extends ModelBase {

    private static final long serialVersionUID = -7086382610728542290L;
    private String title;
    private Long coverPhotoId;

    public AlbumRequest() {
        super();
    }

    public AlbumRequest(String title, Long coverPhotoId) {
        this.setTitle(title);
        this.setCoverPhotoId(coverPhotoId);
    }

    @NotEmpty
    @Length(max = 50)
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Min(value = 1)
    public Long getCoverPhotoId() {
        return this.coverPhotoId;
    }

    public void setCoverPhotoId(Long coverPhotoId) {
        this.coverPhotoId = coverPhotoId;
    }

}