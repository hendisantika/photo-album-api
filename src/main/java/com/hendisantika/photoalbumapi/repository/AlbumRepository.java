package com.hendisantika.photoalbumapi.repository;

import com.hendisantika.photoalbumapi.domain.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:20
 */
public interface AlbumRepository extends JpaRepository<Album, Long>, QueryDslPredicateExecutor<Album> {

}