package com.hendisantika.photoalbumapi.repository;

import com.hendisantika.photoalbumapi.domain.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:38
 */
public interface FileRepository extends JpaRepository<File, Long> {

    File findByFilename(String filename);

}