package com.hendisantika.photoalbumapi.repository;

import com.hendisantika.photoalbumapi.domain.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:38
 */
public interface PhotoRepository extends JpaRepository<Photo, Long>, QueryDslPredicateExecutor<Photo> {

}