package com.hendisantika.photoalbumapi.service;

import com.hendisantika.photoalbumapi.domain.entity.Album;
import com.hendisantika.photoalbumapi.model.request.AlbumRequest;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:40
 */
public interface AlbumService {

    Album createAlbum(AlbumRequest albumRequest);

    Album getAlbumById(Long id);

    Iterable<Album> getAlbums(String search, Pageable pageable);

    Album updateAlbum(Long id, AlbumRequest albumRequest);

    void deleteAlbum(Long id);

}