package com.hendisantika.photoalbumapi.service;

import com.hendisantika.photoalbumapi.domain.entity.Album;
import com.hendisantika.photoalbumapi.domain.factory.AlbumFactory;
import com.hendisantika.photoalbumapi.domain.predicate.builder.AlbumPredicateBuilder;
import com.hendisantika.photoalbumapi.model.request.AlbumRequest;
import com.hendisantika.photoalbumapi.repository.AlbumRepository;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:42
 */
@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumFactory albumFactory;

    @Autowired
    private AlbumRepository albumRepository;

    @Override
    @Transactional
    public Album createAlbum(AlbumRequest albumRequest) {
        Album album = this.albumFactory.create(albumRequest);
        return this.albumRepository.save(album);
    }

    @Override
    public Album getAlbumById(Long id) {
        return this.albumRepository.findOne(id);
    }

    @Override
    public Iterable<Album> getAlbums(String search, Pageable pageable) {
        AlbumPredicateBuilder builder = new AlbumPredicateBuilder();

        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
        }
        BooleanExpression exp = builder.build();

        return this.albumRepository.findAll(exp, pageable).getContent();
    }

    @Override
    @Transactional
    public Album updateAlbum(Long id, AlbumRequest albumRequest) {
        Album album = this.albumRepository.findOne(id);
        BeanUtils.copyProperties(albumRequest, album);
        return this.albumRepository.save(album);
    }

    @Override
    @Transactional
    public void deleteAlbum(Long id) {
        this.albumRepository.delete(id);
    }

}