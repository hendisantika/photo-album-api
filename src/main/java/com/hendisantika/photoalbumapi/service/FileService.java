package com.hendisantika.photoalbumapi.service;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:41
 */
public interface FileService {

    byte[] serveFile(String filename);

    void deleteFile(Long id);
}
