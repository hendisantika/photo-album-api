package com.hendisantika.photoalbumapi.service;

import com.hendisantika.photoalbumapi.domain.entity.File;
import com.hendisantika.photoalbumapi.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:43
 */
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FileRepository fileRepository;

    @Override
    public byte[] serveFile(String filename) {
        File file = this.fileRepository.findByFilename(filename);
        return file.getData();
    }

    @Override
    @Transactional
    public void deleteFile(Long id) {
        this.fileRepository.delete(id);
    }

}