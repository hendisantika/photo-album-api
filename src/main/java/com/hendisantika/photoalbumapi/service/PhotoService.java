package com.hendisantika.photoalbumapi.service;

import com.hendisantika.photoalbumapi.domain.entity.Photo;
import com.hendisantika.photoalbumapi.model.request.PhotoRequest;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : photo-album-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-24
 * Time: 16:42
 */
public interface PhotoService {
    Photo createPhoto(PhotoRequest photoRequest);

    Photo getPhotoById(Long id);

    Iterable<Photo> getPhotos(String search, Pageable pageable);

    Photo updatePhoto(Long id, PhotoRequest photoRequest);

    void deletePhoto(Long id);
}
